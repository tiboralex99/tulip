describe('Uber request ride', () => {
  beforeEach(() => {
    cy.visit('https://www.uber.com/hu/en/');
    cy.contains('Uber').should('be.visible');
  });
  it('Successful ride request', () => {
    cy.wait(2000);
    cy.get('#2').click();
    cy.get('[name="pickup"]').type('Szeged Plaza')
    cy.get('[aria-label="Szeged Plaza, Szeged, Hungary"]').click();
    cy.get('[name="destination"]').type('Szeged Airport');
    cy.get('[aria-label="Szeged Airport, Szeged, Bajai Way, Hungary"]').click();
    cy.get('[text="Request now"]').click();
    cy.url().should('include', 'auth.uber.com');
    cy.get('#PHONE_NUMBER_or_EMAIL_ADDRESS').type('204134363').should('have.value', '204134363');
    //until this point I could automate this process since verification code would be requested. 
  })
})
