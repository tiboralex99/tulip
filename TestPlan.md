# Test Plan
1. Scope

This document will be reviewed by Tulip's developers and will also be approved by them. The timeline for this testing will be maximum 3 days. We are considering manual testing and automation testing. 

2. Test Approach

Both manual and automated testing will be started simultaneously. Test Manager will be responsible for Manual Tester's and Test Automation Engineer's work. The others will be responsible for manual testing and automation testing. Also carrying out their work on time. Automation tool will be cypress.io. Considering bugs hotfixes will be released. Definition of Done: Both testers feel like they covered most of the interface and test coverage is above 80% and all bug tickets are solved. 

3. Test environment

We will work in production since we have no access to source code. 

4. Risk analysis

Downtime from Uber. Both our testers get sick and will not be able to work. Work will be delayed one week. 

5. Review and approvals

Testing will be reviewed and approved by the team's Test Manager.
