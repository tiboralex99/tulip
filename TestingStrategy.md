# Testing Strategy

Reactive Test Strategy is chosen for this project. In this type of test strategy, testing is reactive to the component or system being
tested, and the events occurring during test execution, rather than being pre-planned (as thepreceding strategies are). Tests are designed and implemented, and may immediately be executed in response to knowledge gained from prior test results. Exploratory testing is a common technique employed in reactive strategies. 

Exploratory testing will be done by manual testers. Regression testing, end-to-end testing will be done by automation testers. 