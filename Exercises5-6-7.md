# Suggestions

I've found a bug: based on the documentation I know it is a bug so I create a bug ticket. I need to give it a title that is easy to understand. The ticket needs to be provided with steps to reproduce, expected and actual results. We need to provide environmental variables: Browser version, PC or Mac or mobile, version of app. If needed we can provide screenshot or video of the whole process. 

Quality metrics: 

- user-friendly
- no latency
- interface / progress is working as it was intended to 
- works the same way in all browsers and devices

Test automation:
E2E testing with cypress is suggested. 